<?php 
require_once 'Class_Unite.php';
require_once 'Capital.php';


class General {
	public function __construct($name, $player){
		$this->player = $player;
		array_push($this->player->generals, $this);
		$this->loc = $this->player->capital;
		array_push($this->loc->generals, $this);
		$this->name = $name;
		$this->reputation = 1;
		$this->army = array();
		$this->position = null;
		$this->fortify = False;
	}
	
	public function attack($target){
		$this->position = 'Attack';
		$target->position = 'Defense';
		$groups = array($this, $target);
		$battle = False;
		if (get_class($target) == 'City'){
			foreach ($target->generals as $g){
				if ($g->player != $this->player and $g->fortify == True){
					$g->position = 'Defense';
					array_push($groups, $g);
					$target->battle = True; /*Territoire en guerre*/
					$battle = True;	
				}
			} unset($g);
		}
		elseif count($target->army) > 0){
			$battle = True;
		}
		if ($battle){
			battle($groups);
		}
		elseif (get_class($target) == 'City'){
			$this->player->conquer($target);
		}		
	}

	public function death(){
		array_splice($this->player->generals, $this);
		array_splice($this->loc->generals, $this);
	}
}

function send_line(){
	$A = array_sum(array_map(function($x){return count($x->armee)}, $Attack)) + count($Al);
	$D = array_sum(array_map(function($x){return count($x->armee)}, $Defense)) + count(Dl);
	$M = max(array_map(function($x){return count($x->armee)}, $groups ));
	$m = min(array_map(function($x){return count($x->armee)}, $groups ));
	$n = max(array(1, min(array(7, $m, intdiv($M, 3))) - $wall));
	while($A >= $n and count($Al)<$n){
		for ($i = 0; $i < $n-count($Al); $i++) {
			$a = random_int(0, count($Attack)-1);
			$au = random_int(0, count($Attack($a)->army) -1);
			array_push($Al, $Attack(a)->army(au))
			array_splice($Attack(a)->army, au)
		}
		for ($i = 0; $i < $n-count($Dl); $i++) {
			$d = random_int(0, count($Defense)-1);
			$du = random_int(0, count($Defense($d)->army) -1);
			array_push($Dl, $Defense(d)->army(du))
			array_splice($Defense(d)->army, du)
		}
	}
}

function reputation(){
	return min(2, $D0/(10*count($Attack))) /*Formule arbitraire, à modifier*/
}


function battle($groups){
	Attack = array();
	Defense = array();
	foreach($groups as $obj){
		if (get_class($obj) == 'Capital'){
			$wall = $obj->buildings['Mur'];
		} else {
			$wall = 0;
		}
		if ($obj->position == 'Attack'){
			array_push($Attack, $obj);
		} else {
			array_push($Defense, $obj);
		}
	}unset($obj);
	$Al = array();
	$Dl = array();
	send_line();
	$A0 = $A;
	$D0 = $D;
	while ($A > 0 and $D > 0){
		for ($duel = 0; $duel < $n); $duel++){
			$Al($duel)->attack($Dl($duel));
			$Dl($duel)->attack($Al($duel));
			if ($Al($duel)->HP <= 0 or $Dl($duel)->HP <= 0){
				if ($Al($duel)->HP > 0){
					$Dl($duel)->loc = null;
					array_push($Al($duel)->loc->armee, $Al($duel));
				} else {
					$Al($duel)->loc = null;
					array_push($Dl($duel)->loc->armee, $Dl($duel));
				}
				$Dl($duel) = null;
				$Al($duel) = null;
			}
		}
		if in_array(null, array_merge($Al, $Dl)){
			foreach(array($Al, $Dl) as $L){
				while(in_array(null, $L)){
					array_splice($L, null);
				}
			} unset($L);
			send_line();
		}
	}
	if ($D == 0){
		$losers = $Defense;
		$winners = $Attack;
	} else {
		$losers = $Attack;
		$winners = $Defense;
	}
	foreach ($winners as $obj){
		if (get_class($obj) == 'General'){
			$obj->reputation += reputation();
		}
	} unset($obj);
	foreach ($groups as $obj){
		if (get_class($obj) == 'General' and count($obj->army) == 0){
			$obj->death();
		}
	} unset($obj);
	foreach ($losers as $obj){
		if (get_class($obj) == 'Capital'){
			$obj->suzerain = $winners(0)->player;
			$obj->player->vassalisation();
			array_push($winners(0)->player->vassaux, $obj);
		} elseif (get_class($obj) == 'City'){
			$winners(0)->player->conquer($obj);
		}
	}
}

?>
