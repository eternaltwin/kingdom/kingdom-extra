<?php
require_once 'Class_Unite.php';
require_once 'Grille.php';

class Player{
	public function __construct($name){
		$this->name = $name;
		$this->title = 'Chevalier';
		$this->age = 20;
		$this->gloire = 1;
		$this->apogee = 1;
		$this->commerce = 0;
		$this->generals = array();
		$this->capital = null;
		$this->suzerain = null;
		$this->vassaux = array();
		$this->kingdom = array();
	}
	
	public function conquer($city){
		array_splice($city->player->kingdom, array_search($city, $city->player->kingdom));
		$city->suzerain = $this;
		array_push($this->kingdom, $city);
		$this->gloire = count($this->kingdom);
		if ($this->gloire > $this->apogee){
			$this->apogee = $this->gloire;
		}
		/*A COMPLETER AVEC LE COMMERCE*/
	}
	
	public function vassalisation(){
		foreach($this->vassaux as $v){
			$v->suzerain = null;
		}
		foreach($this->kingdom as $t){
			foreach($t->army as $u){
				$u->barbare = True;
			}
		}
		$this->kingdom = array($this->capital);
		$this->gloire = 1;
	}
}

class City {
	public function __construct($name, $x, $y, $neighbours){
		$this->name = $name;
		$this->loc = array($x,$y);
		$this->neighbours = $neighbours;
		$this->suzerain = null;
		$this->army = array();
		$this->generals = array();
		$this->battle = False;
		$this->position = 'Defense';
		$this->distance = 1; /*A CHANGER AVEC L'ALGO DE TALSI*/
		$this->commerce = 0;
		$this->sc = 0;
	}
	
	public function is_connected($city2){
		$res = False;
		if (in_array($this, $city2->suzerain->kingdom) and in_array($city2, array_keys($this->neighbours))){
			$res = True;
		} else {
			foreach(array_keys($this->neighbours) as $n){
				if (in_array($v, $city2->suzerain->kingdom) and v->is_connected($city2)){
					$res = True;
				}
			}
		}
		return $res
	}

}

?>