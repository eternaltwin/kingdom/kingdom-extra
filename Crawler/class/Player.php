<?php
/**
 * Player object
 */
class Player {
	public $uid;
	public $title;
	public $glory;
	public $reputation;
	public $commerce;
	public $age;
	
	/**
	 * Set player datas
	 * @param array $input Matches from parseRanks() RegExp
	 */
	public function __construct($input) {
		$this->uid =        intval($input['uid']);
		$this->title =      strval($input['title']);
		$this->glory =      intval($input['glory']);
		$this->reputation = intval($input['reputation']);
		$this->commerce =   intval($input['commerce']);
		$this->age =        $this->intAge(trim($input['age']));
	}
	
	/**
	 * Convert aging to decimal value
	 * @param string $str Age in years, month
	 * @return float
	 */
	public function intAge($str){
		$a = 0;
		$b = preg_match('/(?<y>\d+)\sans(?:\set\s(?<m>\d+)\smois)?/', $str, $matches);
		if($b){
			$a = intval($matches['y']);
			if(array_key_exists('m', $matches)){
				$a += intval($matches['m']) / 12;
			}
		}
		return $a;
	}
}