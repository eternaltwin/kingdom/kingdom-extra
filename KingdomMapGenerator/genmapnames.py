#!/usr/bin/env python3

""" Generation of list of map names.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

from namegenerator import NameGenerator
import json
import random

# We do not want to reuse map names from Motion Twin Kingdom
official_kingdom_french_names = [
    'Agereux', 'Ageuille', 'Agigny', 'Agonnet', 'Agovain', 'Angenay', 'Angileux', 
    'Angillarde',  'Angovain', 'Angovette', 'Anguchette', 'Annarleux', 'Annilald', 
    'Annilir', 'Auxarleux', 'Auxerain', 'Auxerand', 'Auxignan', 'Auxigny', 
    'Auxoing', 'Auxovur', 'Azeigne', 'Azigues', 'Azillarde', 'Azirette', 
    'Azuchele', 'Borillarde', 'Borilude', 'Boromald', 'Boruchis', 'Casailir', 
    'Casillarde', 'Echailele', 'Echarde', 'Echarlert', 'Echarles', 'Echarloi', 
    'Echault', 'Echillarde', 'Echirand', 'Echiris', 'Ermaigues', 'Ermarlur', 
    'Ermenay', 'Ermignan', 'Ermiroi', 'Ermiros', 'Gasarland', 'Gasomand', 
    'Genuchuch', 'Guichon', 'Guidetz', 'Guillier', 'Guimois', 'Guines', 
    'Guineuve', 'Isailand', 'Isault', 'Iseigne', 'Isilald', 'Isilas', 'Isoing', 
    'Isonnet', 'Keriras', 'Kerires', 'Keromuch', 'Keronnet', 'Kerovur', 
    'Lomaigues', 'Lomigny', 'Lomigues', 'Lulert', 'Lume', 'Luniere', 'Lunost', 
    'Luscon', 'Maraigues', 'Mararde', 'Marigues', 'Maromain', 'Maromert', 
    'Maromoux', 'Osailoi', 'Osenay', 'Parailette', 'Parigues', 'Parillarde', 
    'Paroing', 'Paroult', 'Poideux', 'Poidoux', 'Poilour', 'Poimild', 'Poinurand', 
    'Poinurette', 'Rachus', 'Racoloi', 'Rafried', 'Ragesnil', 'Ralan', 'Ramoulin', 
    'Ramuys', 'Rases', 'Rochas', 'Rochaye', 'Rodain', 'Rolan', 'Rome', 'Ronastoux', 
    'Ronost', 'Ronoy', 'Rosailain', 'Roseraye', 'Rosovas', 'Rotin', 'Sigarlis', 
    'Sigonne', 'Thibarele', 'Thichon', 'Thicourt', 'Thilir', 'Thimois', 'Thinoy', 
    'Thinurour', 'Thivaux', 'Torailette', 'Torarde', 'Torerele', 'Torignan', 
    'Torillarde', 'Toroult', 'Toroute', 'Toulaigues', 'Touligues', 'Tricoleux', 
    'Tricoloux', 'Tricourt', 'Trifis', 'Trifos', 'Triric', 'Tritos', 'Vaubarir', 
    'Vaulild', 'Vaumirand', 'Vaunureil', 'Vauscon', 'Vauviers', 'Yerieres', 
    'Yeromald', 'Yeroute',
    'Anguchoux', 'Annillarde', 'Annonnet', 'Auxenay', 'Azarleux', 'Casarde', 
    'Echilyac', 'Ermelme', 'Ermoute', 'Ermuchert', 'Guicheil', 'Guifoux', 
    'Guimald', 'Kerarde', 'Kerilette', 'Keroveil', 'Lomelme', 'Lullier', 'Lutin', 
    'Luvaux', 'Mareigne', 'Maroult', 'Osailuch', 'Osiruch', 'Osombes', 'Osovir', 
    'Parailur', 'Parault', 'Parignan', 'Poimirert', 'Poissac', 'Rabette', 'Racq', 
    'Radeluys', 'Rades', 'Ragonnet', 'Ravaux', 'Rocheil', 'Rodeletz', 'Rodis', 
    'Sigoult', 'Thibeux', 'Thideloi', 'Thiduch', 'Thigny', 'Thinost', 'Torereux', 
    'Toroing', 'Touleuille', 'Toulieres', 'Touloute', 'Tridaye', 'Triduch', 
    'Trilan', 'Trissagne', 'Vaumeux', 'Yerilos', 'Yerombes',
    'Agovas', 'Ermailoi', 'Ermoing', 'Genuchain', 'Iseraye', 'Kerarlus', 
    'Lucolild', 'Lutel', 'Luviers', 'Paroute', 'Poicolude', 'Poises', 'Rosault', 
    'Rosoing', 'Thifried', 'Thilan', 'Thileux', 'Toruchis', 'Vaumirain', 
    'Vautoux', 'Yerailoi',
    'Agomeux', 'Rachon'
]

official_kingdom_english_names = [
    'Camiton', 'Liveruchorne', 'Basinguton', 'Plaimirour', 'Romompton', 'Deriton' ,
    'Plaibarave', 'Winingham', 'Liverompton', 'Isingdon', 'Oswingley', 'Plaidary', 
    'Oswuchover', 'Olnailam', 'Avetown', 'Winorough', 'Swinorough', 'Ulgington',
    'Leitorne', 'Penaton', 'Deriliton', 'Tamirert', 'Inverenay', 'Barningham', 
    'Readilere', 'Isailenham', 'Inverorough', 'Barnovord', 'Osworough', 'Alviniton', 
    'Alvinilby', 'Barnomby', 'Swiningdon', 'Romiland', 'Ulgingley', 'Leidelere', 
    'Romomiton', 'Berkesea', 'Isilald', 'Alvinailand', 'Kilidover', 'Osoveton', 
    'Olnaton', 'Inverirby', 'Plaidby', 'Isovesey', 'Abererary', 'Berketand', 
    'Readuchiton', 'Inveruchord', 'Isenay', 'Winompton', 'Leicolall', 'Plaichon', 
    'Readorough', 'Barnerenham', 'Penuchave', 'Penilald', 'Kilidelorne', 'Basingingham', 
    'Camiliton', 'Readailand', 'Beaunastiton', 'Beaumanston', 'Alverailert', 
    'Alviningdon', 'Aguton', 'Isuchby', 'Isilert', 'Leigordon', 'Invererey', 
    'Inverorley', 'Aberucham', 'Winoughton', 'Berkewold', 'Avenaston', 'Derompton', 
    'Beaunock', 'Olnovenham', 'Swinovand', 'Olnerorne', 'Swininster', 'Tamaton',
    'Inveruton', 'Basinguchby', 'Aberuton', 'Barniton', 'Beaufand', 'Aberinster', 
    'Camovand', 'Kilenay', 'Readington', 'Basingarlby', 'Alverilough', 'Alvinilam', 
    'Tamiton', 'Readilough', 'Romington', 'Oswomorne', 'Barnuton', 'Readiton', 
    'Berkewell', 'Alvinovough', 'Kilaton', 'Winerave', 'Liverenay', 'Liverirord', 
    'Beaunastald', 'Avetour', 'Alnarlald', 'Osilesey', 'Oswoughton', 'Kilifald', 
    'Liverington', 'Alnovour', 'Avebley', 'Avedelon', 'Swinilert', 'Leibarald', 
    'Berkebigh', 'Alningdon', 'Plaifing', 'Olnorough', 'Olnomby', 'Leicolald', 
    'Leifield', 'Berkedeling', 'Oswarland', 'Plaimanston', 'Basingerere', 
    'Alvinompton', 'Livererby', 'Aberovand', 'Rominster', 'Kililoch', 'Readiling', 
    'Caminster', 'Penailiton', 'Berkeness', 'Swinerover', 'Alnailald', 
    'Liveringham', 'Plaihaven', 'Olnington', 'Alverenay', 'Osaton', 'Aberiton', 
    'Ulgorough', 'Peningley', 'Beaudeleton', 'Kilimond', 'Readingdon', 
    'Olnuton', 'Barninster', 'Kilihead', 'Ulgomert', 'Olnereld', 'Swinilary', 
    'Oswarlby', 'Kilimanston', 'Winiton', 'Penenay', 'Camereton', 'Basingingley', 
    'Tamomere', 'Osarlon', 'Osoming'
]

official_kingdom_spanish_names = [
    'Herbafat', 'Tysechéjar', 'Malizez', 'Alcenéjar', 'Malacuz', 'Casteloyeld', 
    'Besechix', 'Tysazillo', 'Lordubix', 'Isenar', 'Chasizón', 'Josoyold', 
    'Nuhubost', 'Malizum', 'Herbonillo', 'Alcubuz', 'Nojazato', 'Hozechar', 
    'Yurubéjar', 'Torinum', 'Anterey', 'Pelubez', 'Nojacéjar', 'Bolubás', 
    'Benonia', 'Palmonez', 'Defafen', 'Bolacold', 'Benizen', 'Pelerás', 
    'Antazeto', 'Genenela', 'Ferroneld', 'Cerifigas', 'Yurazen', 'Alcafey', 
    'Genonald', 'Lordonix', 'Antumum', 'Malazeld', 'Huyoney', 'Coboneto', 
    'Tysazeld', 'Urlacald', 'Bolifia', 'Antazia', 'Antafar', 'Hozoneld', 
    'Pelonold', 'Famifez', 'Ferronillo', 'Cerazigas', 'Fortechigas', 'Kelubeto', 
    'Nojubeto', 'Benacigas', 'Toracat', 'Granafato', 'Pelafold', 'Ecroyigas', 
    'Malazost', 'Cerumela', 'Yurenón', 'Dominix', 'Huyafost', 'Cerafuto', 
    'Cobinia', 'Ferrubuto', 'Kelifez', 'Yuracetas', 'Antoyez', 'Chaseryd', 
    'Defenold', 'Huyonón', 'Beserum', 'Iseneto', 'Genonum', 'Ecrinen', 'Nojafat', 
    'Pinoyar', 'Aelerat', 'Tysechald', 'Lordifat', 'Hozinyd', 'Chasoyás', 
    'Ferronón', 'Granoyillo', 'Benumuto', 'Domonat', 'Herbazetas', 'Genacela', 
    'Ecrazuz', 'Urbechela', 'Tysonost', 'Antubold', 'Valazillo', 'Malafuz', 
    'Lorderoz', 'Ecrizás', 'Kelazyd', 'Partinar', 'Boloyéjar', 'Chasifen', 
    'Fortinold', 'Ferrinillo', 'Isechald', 'Palmacey', 'Josafyd', 'Granubez', 
    'Lorderuz', 'Garderigas', 'Malubey', 'Valuboz', 'Malazela', 'Ponteryd', 
    'Ecrifuto', 'Ecrafato', 'Cerizeld', 'Antonia', 'Tyserat', 'Huyonás', 
    'Hozaceld', 'Malinyd', 'Palmizost', 'Garderéjar', 'Tukafás', 'Urboyen', 
    'Geninillo', 'Ferrafum', 'Defifix', 'Bolechost', 'Partoyeto', 'Lordifetas', 
    'Gardinar', 'Iseruz', 'Yurifen', 'Nojubold', 'Hozechix', 'Bolubald', 
    'Famifón', 'Ogroyuto', 'Palmumillo', 'Ontonato', 'Generela', 'Ontechix', 
    'Malubigas', 'Pinafillo', 'Josacald', 'Alcoyix', 'Bolenillo', 'Herbinuto', 
    'Cobenum', 'Tysizoz', 'Pinechetas', 'Ferroyuto', 'Onterost', 'Nuhoyeto', 
    'Ecrazás', 'Genizey', 'Ecrubia', 'Benifez', 'Urbubuz', 'Castelifeld', 
    'Lordacat', 'Ontifold', 'Ferrafez', 'Ogrenyd', 'Granizar', 'Defumás', 
    'Urlafetas', 'Tysubum', 'Castelubum', 'Pelechat', 'Besuméjar', 'Nojafez', 
    'Pontumela', 'Genafat', 'Gardonéjar', 'Tukoyat', 'Herbumuto', 'Granoyez', 
    'Lordonillo', 'Besumeld', 'Urlazix', 'Ferrizetas', 'Hozenetas', 
    'Alcifum', 'Gardechia', 'Genenez', 'Defafillo', 'Malinold', 'Bolechigas', 
    'Partifen', 'Pinoyoz', 'Gardubat', 'Isifix', 'Kyminela', 'Lorderéjar', 
    'Kymechia', 'Cobizuto', 'Castelazillo', 'Pinacillo', 'Josonix', 'Pelinuto', 
    'Kymubey', 'Ogronéjar', 'Urlizuto', 'Pelizás', 'Domeneld', 'Ecrenez', 
    'Tysafeld', 'Ecrumetas', 'Alcoyuto', 'Fortizuz', 'Nuhacey', 'Nuhazia', 
    'Aelifey', 'Yurinéjar', 'Fortoyar', 'Benubald', 'Lorderuto', 'Cobechigas', 
    'Gardumia', 'Yurafald', 'Fortenold', 'Coberez', 'Gardonás', 'Urlafat', 
    'Kymoyum', 'Tukubás', 'Tysonón', 'Herbubez', 'Aelaceld', 'Pontubillo', 
    'Kelinela', 'Besubold', 'Maliney', 'Yurafela', 'Yuronum', 'Partenar', 
    'Genizillo', 'Hozinoz', 'Hozoyuto', 'Cerenen', 'Toreneto', 'Fortenón', 
    'Fortiféjar', 'Chaseréjar', 'Coboney', 'Aelenoz', 'Domafetas', 'Isacum', 
    'Huyonost', 'Tysumost', 'Famereto', 'Ontumald', 'Domifeld', 'Chasizald', 
    'Alcechela', 'Herbazeld', 'Josubyd', 'Tukifuz', 'Toronuz', 'Gardumuto', 
    'Urbereld', 'Urbinat', 'Valizuz', 'Nojonuto', 'Tysubyd', 'Kymafum', 
    'Domafigas', 'Alcinen', 'Cobumez', 'Yurechia', 'Huyoyéjar', 
    'Bolafia', 'Partechato', 'Urlubia', 'Josecheld', 'Urbenigas', 'Castelazoz', 
    'Yuracón', 'Tysonyd', 'Herbubéjar', 'Urlubuz', 'Nojinigas', 'Tysizuto', 
    'Josizás', 'Gardacez', 'Defifald', 'Casteloyum', 'Granazillo',  
    'Bolubigas', 'Kelazeld', 'Bolenar', 'Tysizum', 'Ecrizuz', 'Defifato', 
    'Genifix', 'Besoyost', 'Gardazela', 'Ceroyillo', 'Palmazix', 'Ecrumald', 
    'Definetas', 'Aelerillo', 'Genizald', 'Nuhubela', 'Kelifetas', 'Huyafela', 
    'Alcacost', 'Ecrafald', 'Pontafia', 'Ecrazald', 'Famizey', 'Pontazold', 
    'Tukaféjar', 'Cererey', 'Domafen', 'Urlafillo', 'Hozinix', 'Urbifost', 
    'Defafigas', 'Ecrifum', 'Ontazost', 'Lordifez', 'Defizón', 'Hozerost', 
    'Lordizela', 'Isacato', 'Ferrizey', 'Ferreney', 'Garderillo', 'Granonen', 
    'Tysonás', 'Benineto', 'Ferronuto', 'Ogroneto', 'Ferrineld', 'Pelereld', 
    'Malizen', 'Definoz', 'Besumey', 'Urbifetas', 'Fortizato', 
    'Gardafez', 'Lordumato', 'Ecrifillo', 'Isacat', 'Ograzeto', 'Chasifuto', 
    'Nuhoyón', 'Partafuz', 'Malinuto', 'Pinumeld', 'Alcerat', 'Isumela', 
    'Cerazoz', 'Urloyillo', 'Tysifez', 'Urlubey', 'Pelonás', 'Herbechold', 
    'Cobazix', 'Aelechoz', 'Hozafat', 'Alcazetas', 'Casteliney', 'Ecrenia', 
    'Domoyeld', 'Genenillo', 'Ecrifez', 'Famaceto', 'Fortubás', 'Domumost', 
    'Valubuto', 'Tukacuto', 'Valenum', 'Pelazold', 'Castelerald', 'Huyumuz', 
    'Nojonat', 'Fortinás', 'Cobazuz', 'Nuhazat', 'Partizia', 'Kymifigas', 
    'Famonigas', 'Malumat', 'Herbacyd', 'Isinald', 'Palmafez', 'Valaceld', 
    'Partenuz', 'Domifia', 'Beseretas', 'Toroyix', 'Tukonoz', 'Tyserigas', 
    'Ecrenuz', 'Pelafia', 'Urloyix', 'Genifald', 'Fortazás', 'Yuroyato', 
    'Lordafat', 'Nojerum', 'Malubum', 'Chasechar', 'Palmazat', 'Valoyeto', 
    'Herboyillo', 'Genacuto', 'Torumato', 'Peleruz', 'Famenat', 'Pontumato', 
    'Besinela', 'Graninuz', 'Josonás', 'Forterold', 'Domechuto', 'Granazez', 
    'Isinat', 'Besonetas', 'Tukacela', 'Antaceld', 'Isifold', 'Malenez', 
    'Aelifeto', 'Huyerás', 'Cerizéjar', 'Hozafón', 'Castelifeto', 'Urberón', 
    'Pelifost', 'Valifat', 'Josumez', 'Genonuto', 'Defazás', 'Nojeretas', 
    'Defonás', 'Ponterar', 'Josubez', 'Castelazez', 'Famumeld', 'Aelubix', 
    'Benoyum', 'Famonoz', 'Pelenigas', 'Famumold', 'Famifoz', 'Kelinen', 
    'Palmacald', 'Besiféjar', 'Defifuto', 'Pinumar', 'Tysazez', 'Antubyd', 
    'Genumey', 'Toracéjar', 'Famoyald', 'Antizato', 'Huyonen', 'Isechillo', 
    'Kelenéjar', 'Benereld', 'Fortonia', 'Ferroyum', 'Fortafetas', 
]

official_kingdom_german_names = [
    'Tripoldisholz', 'Jecholda', 'Wurstschweng', 'Ebersgrub'
]


def main():

    french_names = NameGenerator("french").gen_all_city_names()
    english_names = NameGenerator("english").gen_all_city_names()
    spanish_names = NameGenerator("spanish").gen_all_city_names()
    german_names = NameGenerator("german").gen_all_city_names()

    for official_name in official_kingdom_french_names:
            french_names.remove(official_name)
    for official_name in official_kingdom_english_names:
            english_names.remove(official_name)
    for official_name in official_kingdom_spanish_names:
        spanish_names.remove(official_name)
    for official_name in official_kingdom_german_names:
            german_names.remove(official_name)

    random.shuffle(french_names)
    random.shuffle(english_names)
    random.shuffle(spanish_names)
    random.shuffle(german_names)

    with open('map_names.json', 'w') as outfile:
            outfile.write(json.dumps({ \
                "french": french_names[:200], \
                "english": english_names[:200], \
                "spanish": spanish_names[:200], \
                "german": german_names[:200], \
                },\
                ensure_ascii=False, separators=(',', ':')))
    
if __name__ == "__main__":
    main()
