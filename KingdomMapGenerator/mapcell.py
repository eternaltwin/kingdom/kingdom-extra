"""
This module manages landscape images in cells. A cell is a part of the part delimited by paths.
Each cell is associated to a landform, and we add images related to this landform.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import random

class MapCell:
    def __init__(self, cell_cities):
        self._cities = cell_cities
        self._area = self._compute_area()
        self._landform = self._determine_landform()

    @property
    def cities(self):
        return self._cities

    @property
    def area(self):
        return self._area

    @property
    def landform(self):
        return self._landform

    def _compute_area(self):
        """Compute the area of a cell.

        Returns:
            float: the (algebric) area of the cell. The boundary cell has a negative area, others are positive.
        """
        #https://stackoverflow.com/questions/34326728/how-do-i-calculate-the-area-of-a-non-convex-polygon
        area = 0
        p1 = self._cities[-1]
        for p2 in self._cities:
            area += p1.y * p2.x - p1.x * p2.y
            p1 = p2
        return 0.5 * area

    def _determine_landform(self):
        if self._area < 0.:
            return "mountains"
        
        if self._area > random.randrange(900, 1100):
            k = random.randrange(0, 3)
            if k == 0:
                return "mountains"
            elif k == 1:
                return "snowy_mountains"
            else:
                return "volcanos"

        if self._area < 120:
            return "dirt"
        if random.randrange(0, 30) == 0:
            return "sand"
        
        x_list = [city.x for city in self._cities]
        y_list = [city.y for city in self._cities]

        delta_x = max(x_list) - min(x_list)
        delta_y = max(y_list) - min(y_list)

        if self._area > 200 and delta_x > 5 and delta_y > 5 and delta_x * delta_y < 2. * self._area and random.randrange(0,4):
            return "lake"

        if random.randrange(0, 3) == 0:
            return "forest"
        return "hills"
