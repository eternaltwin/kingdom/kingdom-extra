"""
This module implements the KingdomMap class, the main class of the map generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json

from mapgraph import MapGraph


class MyEncoder(json.JSONEncoder):
    def encode(self, obj):

        if isinstance(obj, dict):
            result = '{'
            for key, value in obj.items():
                if isinstance(value, float):
                    encoded_value = format(value, '.2f')
                elif isinstance(value, dict) or isinstance(value, list):
                    encoded_value = MyEncoder.encode(self, value)
                else:
                    encoded_value = json.JSONEncoder.encode(self, value)

                result += f'"{key}":{encoded_value},'

            result = result[:-1] + '}'
            return result
        return json.JSONEncoder.encode(self, obj)
    

class KingdomMap:
    """KingdomMap is the class that represents a map in Kingdom.
    """
    def __init__(self, language, map_type, width, height):
        """Create an empty map

        Args:
            width (int): the width of the map in pixels
            height (int): the height of the map in pixels
        """
        self._width = width
        self._height = height
        self._map_type = map_type
        self._language = language
        self._map_graph = MapGraph()


    def generate(self, **kwargs):
        """Generate the map graph: the cities position and the links between cities.
        """
        self._map_graph.generate(self._language, self._width, self._height, **kwargs)


    def load_graph_from_json(self, json_filename, map_margin):
        """Load a map graph from a json file.

        Args:
            json_filename (string): path to the json file defining the map graph
            map_margin (int): a margin around the map
        """
        with open(json_filename) as f:
            map_data = json.load(f)
        self._width = map_data["map_width"]
        self._height = map_data["map_height"]
        self._map_graph.load_cities_from_json(json_filename, map_margin)

    @property
    def width(self):
        """
        Returns:
            int: the width of the map in pixels
        """
        return self._width

    @property
    def height(self):
        """
        Returns:
            int: the heigth of the map in pixels
        """
        return self._height

    @property
    def map_graph(self):
        """
        Returns:
            MapGraph: the map graph associated to the map
        """
        return self._map_graph

    def export_to_json(self, json_file_name):
        voronoi_vertices = []
        for vertex in self._map_graph.voronoi_vertices:
            voronoi_vertices.extend(vertex)

        with open(json_file_name, 'w') as outfile:
            outfile.write(json.dumps({ \
                "language": self._language, \
                "map_type": self._map_type, \
                "map_width": self.width, \
                "map_height": self.height, \
                "cities": self._map_graph.export_cities(),\
                "landforms": self._map_graph.landforms_triangles,\
                "distances": self._map_graph.compute_distances(),\
                "paths_length": self._map_graph.compute_paths_length(), \
                "voronoi_vertices": voronoi_vertices,\
                'voronoi_indices': [region["vertices"] for region in self._map_graph.voronoi_regions.values()], \
                },\
                cls=MyEncoder, ensure_ascii=False, separators=(',', ':')))
    
    def export_graph(self, json_file_name):
        """Export the map graph into a json file.

        Args:
            json_file_name (string): path to the json file
        """
        serial = self._map_graph.export_cities()
        with open(json_file_name, 'w') as outfile:
            outfile.write(json.dumps(serial, ensure_ascii=False, separators=(',', ':')))        
