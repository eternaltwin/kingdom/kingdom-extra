"""
Tools to analyse a map after generation.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

def check_links_consistency(cities):
    """Check if when there is a link from a city A to a city B, there is also a link from B to A.

    Args:
        cities (list of Cities): the list of the cities in the map

    Returns:
        bool: true if every link is reciprocal
    """
    for city in cities:
        if city in city.adjacent_cities:
            return False
        for adj_city in city.adjacent_cities:
            if not city in adj_city.adjacent_cities:
                return False
    return True


def print_statistics(cities):
    """Prints in console useful statistics about the map

    Args:
        cities (list of Cities): the list of the cities in the map
    """
    nb_cities = len(cities)
    nb_capitals = len([city for city in cities if city.is_capital])

    if nb_capitals :
        ratio = nb_cities / nb_capitals
        print("The map has",nb_cities, "cities, including",nb_capitals, "capitals. Ratio:",ratio,".")

    for i in range(1,5):
        nb_with_i_link = len([city for city in cities if len(city.adjacent_cities) == i])
        print("Number of cities with", i, "links:", nb_with_i_link)

