"""
Manage the graph of a map: the cities and the links between them.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json
import math
import random

from city import City
from namegenerator import NameGenerator
from mapcell import MapCell
from voronoi import Voronoi, Delaunay


class MapGraph:
    """The graph of the map: cities and links between them.
    """
    def __init__(self):
        """Create an empty graph
        """
        self._cities = list()
        self._city_to_id = dict()

    @property
    def cities(self):
        """
        Returns:
            list of City: the cities of the graph
        """
        return self._cities

    @property
    def voronoi_vertices(self):
        """
        Returns:
            list of list of int: coordinates of the vertices of voronoi cells
        """
        return self._voronoi_vertices

    @property
    def voronoi_regions(self):
        """
        Returns:
            dict: keys are cities ids.
            Values contains the list of vertices, and the list of adjacent regions (identified by their city id)        """
        return self._voronoi_regions

    @property
    def landforms_triangles(self):
        return self._landforms_triangles


    def load_cities_from_json(self, json_filename, map_margin):
        with open(json_filename) as f:
            json_data = json.load(f)
        cities_data_list = json_data["cities"]
        self._cities = [City(city_data["x"], city_data["y"], city_data["cap"], city_data["name"]) for city_data in cities_data_list]
        for city_data, city in zip(cities_data_list, self._cities):
            city._adjacent_cities = [self._cities[city_id] for city_id in city_data["adj"]]
        map_width = json_data["map_width"]
        map_height = json_data["map_height"]
        self._compute_voronoi(map_width, map_height, map_margin / 2)
        self._find_cells(map_width, map_height)    
        self._compute_landforms_triangles()
        self._update_city_to_id_dict()

    def load(self, cities_data_list, language, map_width, map_height, margin):
        """Loads a graph from a list of cities

        Args:
            cities_data_list (list of City): the cities of the graph
            language (string): map language (for name generation)
            map_width (int): width of the map in pixels
            map_height (int): height of the map in pixels
            margin (int): number of pixels on the side of the map without any city
        """
        name_gen = NameGenerator(language)
        self._cities = [City(city_data["x"], city_data["y"], name_gen, city_data["cap"]) for city_data in cities_data_list]

        for (city_data, city) in zip(cities_data_list, self._cities):
            city.name = city_data["name"]
            for adj in city_data["adj"]:
                city.add_link_to_city(self._cities[adj])

        self._sort_links_ccw()
        self._compute_voronoi(map_width, map_height, margin/2)
        self._update_city_to_id_dict()


    def export_cities(self):
        """Export cities data

        Returns:
            list of dict: each element of the list contains the data for a City
        """
        cities_list = list()

        for city in self._cities:
            city_item = { \
                'x': city.x,\
                'y': city.y,\
                'cap': city.is_capital,\
                'name': city.name,\
                'adj': [self._city_to_id[adj_city] for adj_city in city.adjacent_cities]
            }
            cities_list.append(city_item)
        
        return cities_list

    def generate(self, language, map_width, map_height, **kwargs):
        """Generate the map the graph of the map: 
            - cities and capitals position with their name 
            - links between cities
            - voronoi cells
        """
        name_gen = NameGenerator(language)
        margin = kwargs.get("margin", 128)
        sample_coeff = kwargs.get("sample_coeff", 1920)
        min_dist = kwargs.get("min_dist_between_cities", 80)
        max_dx = kwargs.get("max_dx", 240)
        max_dy = kwargs.get("max_dy", 192)
        max_dist_between_path_and_city = kwargs.get("max_dist_between_path_and_city", 64)
        max_long_edge_criteria = kwargs.get("max_long_edge_criteria", 240)

        self._random_gen_cities(map_width, map_height, margin, sample_coeff, min_dist)
        self._triangulate(map_width, map_height, max_dx, max_dy)
        self._remove_too_near_edges(max_dist_between_path_and_city)
        self._remove_long_edges(max_long_edge_criteria)
        self._sort_links_ccw()
        self._limit_adjacency(4)
        self._delete_disconnected_cities()
        self._place_capitals(name_gen)     
        self._compute_voronoi(map_width, map_height, margin/2)
        self._find_cells()    
        self._compute_landforms_triangles()
        self._update_city_to_id_dict()
 
    def compute_distances(self):
        """"Compute distances between adjacent cities
        
        Returns:
            dict of dict of float: distances between two adjacent cities 
        """
        distances = {}
        for i, city in enumerate(self._cities):
            if i not in distances.keys():
                distances[i] = {}
            for adj_city in city.adjacent_cities:
                adj_id = self._city_to_id[adj_city]
                if adj_id > i:
                    dx = city.x - adj_city.x
                    dy = city.y - adj_city.y
                    distance = math.sqrt(dx ** 2 + dy ** 2)
                    if adj_id not in distances:
                        distances[adj_id] = {}
                    distances[i][adj_id] = distance
                    distances[adj_id][i] = distance
        return distances
    
    def compute_paths_length(self):
        paths_lenght = {}
        for i_capital, capital in enumerate(self._cities):
            if not capital.is_capital:
                continue
            paths_lenght[i_capital] = {}
            unvisited_cities = self._cities.copy()
            unvisited_cities.remove(capital)
            current_step_cities = capital.adjacent_cities
            length = 1
            while len(unvisited_cities):
                next_step_cities = []
                for city in current_step_cities:
                    unvisited_cities.remove(city)
                    if not city.is_capital:
                        i_city = self._city_to_id[city]
                        paths_lenght[i_capital][i_city] = length
                    for adj_city in city.adjacent_cities:
                        if adj_city in unvisited_cities and adj_city not in current_step_cities and adj_city not in next_step_cities:
                            next_step_cities.append(adj_city)
                current_step_cities = next_step_cities
                length += 1
        return paths_lenght

    def _update_city_to_id_dict(self):
        self._city_to_id = {city: city_id for city_id,city in enumerate(self._cities)}


    def _random_gen_cities(self, map_width, map_height, margin, sample_coeff, min_dist):
        nb_x_chunks = (map_width - 2 * margin) // min_dist + 1
        nb_y_chunks = (map_height - 2 * margin) // min_dist + 1
        chunks = []
        for i in range(nb_x_chunks):
            chunks.append([])
            for j in range(nb_y_chunks):
                chunks[i].append([])

        for _ in range(((map_width - 2 * margin) * (map_height - 2 * margin)) // sample_coeff):
            x = random.randrange(0, map_width - 2 * margin) + margin
            y = random.randrange(0, map_height - 2 * margin) + margin
            city = City(x, y)
            i_city = (x - margin) // min_dist
            j_city = (y - margin) // min_dist

            is_position_ok = True
            for i in range(max(0, i_city - 1), min(nb_x_chunks, i_city + 2)):
                for j in range(max(0, j_city - 1), min(nb_y_chunks, j_city + 2)):
                    for other_city in chunks[i][j]:
                        if city.dist2_to_city(other_city) < min_dist ** 2:
                            is_position_ok = False
                            break
                    if not is_position_ok:
                        break
                if not is_position_ok:
                    break
            if is_position_ok:
                chunks[i_city][j_city].append(city)
        
        for i in range(nb_x_chunks):
            for j in range(nb_y_chunks):
                for city in chunks[i][j]:
                    self._cities.append(city)


    def _triangulate(self, map_width, map_height, max_dx, max_dy):
        delaunay = Delaunay(map_width, map_height)
        for i, city in enumerate(self._cities):
            delaunay.add_point(i, [city.x, city.y])

        for triangle in delaunay.triangles:
            if triangle.v[0]>=0 and triangle.v[1]>=0 and triangle.v[2]>=0:
                city_1 = self._cities[triangle.v[0]]
                city_2 = self._cities[triangle.v[1]]
                city_3 = self._cities[triangle.v[2]]
                city_1.add_link_to_city(city_2)
                city_1.add_link_to_city(city_3)
                city_2.add_link_to_city(city_3)

        for city in self._cities:
            for adj_city in city.adjacent_cities:
                dx = adj_city.x - city.x
                dy = adj_city.y - city.y
                if (dx > max_dx) or (dy > max_dy): 
                    city.remove_link_to_city(adj_city)


    def _remove_too_near_edges(self, max_dist_between_path_and_city):
        for city in self._cities:
            for city_1 in self._cities:
                if city == city_1:
                    continue
                links_to_remove = []
                for city_2 in city_1.adjacent_cities:
                    if city == city_2:
                        continue

                    dx = city_2.x - city_1.x
                    dy = city_2.y - city_1.y
                    r = ((city.x - city_1.x) * dx + (city.y - city_1.y) * dy) / (dx * dx + dy * dy)
                    if (r < 0) or (r > 1):
                        continue

                    px = city.x - (city_1.x + dx * r)
                    py = city.y - (city_1.y + dy * r)
                    d2 = px * px + py * py

                    if d2 < max_dist_between_path_and_city ** 2:
                        links_to_remove.append(city_2)
                    
                for city_2 in links_to_remove:
                    city_1.remove_link_to_city(city_2)


    def _remove_long_edges(self, max_long_edge_criteria):
        for city in self._cities:
            if len(city.adjacent_cities) <= 2:
                continue
            links_to_remove = []
            max_links_to_remove = max(len(city.adjacent_cities) - 2, 0)
            for adj in city.adjacent_cities:
                if len(links_to_remove) >= max_links_to_remove:
                    break
                if (city.dist2_to_city(adj) > max_long_edge_criteria ** 2) and random.randrange(0, 4):
                    links_to_remove.append(adj)
            for adj in links_to_remove:
                city.remove_link_to_city(adj)


    def _sort_links_ccw(self):
        """For each City, sorts adjacent (linked) cities to make them in counter clockwise order
        """
        for city in self._cities:
             city.sort_links_ccw()


    def _limit_adjacency(self, max_adjacency):
        """Remove links so that each city has not more than max_adjacency links

        Args:
            max_adjacency (int)
        """
        for city in self._cities:
            while len(city.adjacent_cities) > max_adjacency:
                city.remove_a_link()
    

    def _remove_city(self, city):
            """Remove a city from the graph.

            Args:
                city (City): The City to remove.
            """
            for adj in city.adjacent_cities:
                adj.remove_link_to_city(city)        
            self._cities.remove(city)


    def _delete_disconnected_cities(self):
        """Partition the Graph with sets of (directly or indirectly) connected cities. Keep only the largest set, 
        remove the other cities. So there is a path between every couple of remaining cities.
        """
        unvisited_cities = self._cities.copy()
        partitions = list()

        while len(unvisited_cities):
            current_partition = list()
            next_step_cities = [unvisited_cities.pop()]

            while len(next_step_cities):
                current_partition += next_step_cities
                current_step_cities = next_step_cities
                next_step_cities = list()
                for city in current_step_cities:
                    for adj in city.adjacent_cities:
                        if adj in unvisited_cities:
                            next_step_cities.append(adj)
                            unvisited_cities.remove(adj)
            partitions.append(current_partition)

        len_main_partition = max([len(partition) for partition in partitions])
        main_found = False
        for partition in partitions:
            if len(partition) < len_main_partition or (main_found and len(partition) == len_main_partition):
                for city in partition:
                    self._remove_city(city)
            
    def _place_capitals(self, name_gen):
        nb_capitals = 0
        for city in self._cities:
            if len(city.adjacent_cities) >=3:
                ok = True
                for adj_1 in city.adjacent_cities:
                    if adj_1.is_capital:
                        ok = False
                        break
                    for adj_2 in adj_1.adjacent_cities:
                        if adj_2.is_capital and random.randrange(0, 4):
                            ok = False
                            break
                if ok:
                    city.is_capital = True
                    nb_capitals += 1
            if city.is_capital:
                city.name = name_gen.gen_capital_name()
            else:
                city.name = name_gen.gen_city_name() 


    def _compute_cell_area(self, cell_cities):
        """Compute the area of a cell.

        Args:
            cell_cities (list of City): cities at the vertices of the cell

        Returns:
            float: the (algebric) area of the cell. The boundary cell has a negative area, others are positive.
        """
        #https://stackoverflow.com/questions/34326728/how-do-i-calculate-the-area-of-a-non-convex-polygon
        area = 0
        p1 = cell_cities[-1]
        for p2 in cell_cities:
            area += p1.y * p2.x - p1.x * p2.y
            p1 = p2
        return 0.5 * area

    def _compute_voronoi(self, map_width, map_height, margin):
        """Compute voronoi cells for each city

        Args:
            map_width (int): the width of the map
            map_height (int): the height of the map
            margin (int): number of pixels on the side of the map without any city
        """
        voronoi = Voronoi(self._cities, map_width, map_height, margin)
        self._voronoi_vertices = voronoi.coordinates
        self._voronoi_regions = voronoi.regions 

        self._delaunay_triangles = voronoi.delaunay_triangles

    def _find_cells(self):
        link_visit = dict()
        cells = list()
        for city in self._cities:
            for adj in city.adjacent_cities:
                link_visit[(city, adj)] = False
 
        remaining_links = [link for link, value in link_visit.items() if not value]
        while len(remaining_links):
            c1, c2 = remaining_links[0]
            cell_cities = [c1]

            while True:
                link_visit[(c1, c2)] = True
                search_pos = c2.adjacent_cities.index(c1)
                c1 = c2
                c2 = c2.adjacent_cities[search_pos - 1]

                if c1 == cell_cities[0] and c2 == cell_cities[1]:
                    break
                cell_cities.append(c1)

            cells.append(MapCell(cell_cities))
            link_visit[(c1, c2)] = True
            remaining_links = [link for link, visited in link_visit.items() if not visited]

        self._cells = sorted(cells, key=lambda cell: cell.area)


    def _compute_landforms_triangles(self):
        self._landforms_triangles = {}
        city_to_id = {city: city_id for city_id,city in enumerate(self._cities)}

        remaining_triangles = [triangle.v for triangle in self._delaunay_triangles]

        for cell in self._cells[1:]:
            if cell.landform not in self._landforms_triangles.keys():
                self._landforms_triangles[cell.landform] = []
            cell_cities_ids = [city_to_id[city] for city in cell.cities]

            found_triangles = []
            for triangle in remaining_triangles:
                triangle_found = False
                if triangle[0] in cell_cities_ids:
                    first_vertex_ids = [j for j, cell_city in enumerate(cell_cities_ids) if cell_city == triangle[0]]
                    for i in first_vertex_ids:
                        v = 1
                        for city_id in (cell_cities_ids[i+1:] + cell_cities_ids[:i]):
                            if triangle[v] == city_id:
                                v += 1
                            if v == 3:
                                triangle_found = True
                                break
                        if triangle_found:
                            break
                if triangle_found:
                    found_triangles.append(triangle)
                    self._landforms_triangles[cell.landform].extend(triangle)
            for triangle in found_triangles:
                remaining_triangles.remove(triangle)
