# Kingdom - Commerce simulation

Outil externe au jeu Kingdom ou eKingdom.

## Description

Pour estimer le nombre de tours et le nombre d'unités à investir, pour atteindre un niveau de commerce sur un territoire. 

## Getting Started

### Dependencies

Chart.js https://www.chartjs.org/

### Usage

kingdom.patatoide.com/simulateur.html

## Help

Renseignez le niveau de commerce de départ ainsi qu'un nombre d'unités déployées. Clic sur *Simuler*

Cliquer sur le drapeau pour définir le commerce de départ en fonction de sa distance avec la capitale.

Renseigner un niveau de commerce à atteindre pour déterminer l'objectif. 

## Authors

nzun/abzolument
[@nzun](https://twinoid.com/user/111586)

## Version History

* 0.1
    * Initial Release

## Sources

* Aide de jeu (officielle) : [Archive](http://kingdom.patatoide.com/archives/aide.html)
* Simulateur de commerce par basicbrute et simoons [Groupe IPK Twinoid](https://twinoid.com/g/ipk#simulateur-de-commerce)
* Doc Eternal-Twin du projet eKingdom : https://gitlab.com/eternaltwin/kingdom/kingdom/-/wikis/Documentation/overview
* Twinpedia : commerce [Archive](https://twin.tithom.fr/muxxu/kingdom/commerce/#dhkold)

## License

Graphics/icons courtesy Motion-Twin under CC-By-SA-NC License
